#
# Dockerfile for security-services/code-checking
#
# Maintainer:
# CERN IT-DI-CSO <computer.security@cern.ch>
#

#
# Base image and image metadata
#
FROM centos:latest
MAINTAINER "CERN IT-DI-CSO <computer.security@cern.ch>"

#
# Install with YUM:
# - Epel Repo
# - Basic development tools (3)
# - Java development packages
# - Perl development packages
# - Python development packages (2)
# - Ruby development packages
# - Python PIP
# - General useful software (3)
# - Dependencies for RATS (2: expat expat-devel & flex flex-devel)
# - Perl::Critic
# - PyChecker
# - PyLint
#
RUN yum -y install epel-release && \
    yum -y update && \
    yum -y groups mark convert && \
    yum -y groupinstall 'Development Tools' && \
    yum -y install java-1.8.0-openjdk java-1.8.0-openjdk-devel && \
    yum -y install perl perl-devel && \
    yum -y install python python-devel && \
    yum -y install python-flake8 python-pep8 && \
    yum -y install ruby ruby-devel && \
    yum -y install python-pip && \
    yum -y install wget unzip unrar sudo && \
    yum -y install p7zip p7zip-plugins && \
    yum -y install htop vim && \
    yum -y install expat expat-devel && \
    yum -y install flex flex-devel && \
    yum -y install perl-Perl-Critic && \
    yum -y install pychecker && \
    yum -y install pylint && \
    yum -y clean all


#
# Install CppLint
#
RUN pip install --upgrade pip && pip install cpplint

WORKDIR /opt

#
# Install FindBugs
#
RUN wget http://prdownloads.sourceforge.net/findbugs/findbugs-3.0.1.tar.gz && \
    tar -xzf findbugs-3.0.1.tar.gz && \
    rm -f findbugs-3.0.1.tar.gz && \
    echo '/opt/findbugs-3.0.1/bin/findbugs "$@"' >> /usr/local/bin/findbugs && \
    chmod +x /usr/local/bin/findbugs

#
# Install FlawFinder
#
RUN wget http://www.dwheeler.com/flawfinder/flawfinder-1.31.tar.gz && \
    tar -xzf flawfinder-1.31.tar.gz && \
    rm -f flawfinder-1.31.tar.gz && \
    cd flawfinder-1.31 && \
    make install && \
    cd ..

#
# Install PMD (and CPD)
#
RUN wget https://github.com/pmd/pmd/releases/download/pmd_releases%2F5.5.1/pmd-bin-5.5.1.zip && \
    unzip pmd-bin-5.5.1.zip && \
    rm -f pmd-bin-5.5.1.zip && \
    echo '/opt/pmd-bin-5.5.1/bin/run.sh cpd "$@"' >> /usr/local/bin/cpd && \
    echo '/opt/pmd-bin-5.5.1/bin/run.sh pmd "$@"' >> /usr/local/bin/pmd && \
    chmod +x /usr/local/bin/cpd && \
    chmod +x /usr/local/bin/pmd


#
# Install RATS
#
RUN wget http://fossies.org/linux/privat/rats-2.4.tgz && \
    tar -xzf rats-2.4.tgz && \
    rm -f rats-2.4.tgz && \
    cd rats-2.4 && \
    ./configure && \
    make && \
    make install && \
    cd ..

#
# Finish
#
WORKDIR /
