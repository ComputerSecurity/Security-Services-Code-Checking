#!/bin/sh
#
# Script to build Docker image from Dockerfile
#
# Maintainer:
# CERN IT-DI-CSO <computer.security@cern.ch>
#

#
# Build parameters
#
REPOSITORY='security-services'
IMAGENAME='code-checking'
IMAGETAG="${1:-$( date +'%Y-%m-%d' )}"


#
# Display help, if needed
#
if [[ "${1}" == '-h' || "${1}" == '--help' || "${#}" > 1 ]]; then
    echo 'Usage:'
    echo "  ${0} [TAG]"
    echo "  ${0} [-h|--help]"
    echo ''
    echo 'Arguments:'
    echo '  TAG        -- tag string for image, current date by default'
    echo '  -h|--help  -- display information about usage'
    echo ''
    echo 'Please, note that Docker service must be running on host.'
    exit 0
fi


#
# Display image tag
#
echo "Building image with tag: ${IMAGETAG}"
echo ''


#
# Find and delete any previous image with specified tag
#
PREVIOUS=$( docker images \
            | grep "${REPOSITORY}/${IMAGENAME}" \
            | grep -e "${IMAGETAG}" \
            | awk '{ print $3; }' \
            | sort --unique \
            | tr '\n' ' ' \
            | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' )

if [[ ! -z "${PREVIOUS}" ]]; then
    docker rmi --force "${PREVIOUS}"
fi


#
# Build the image
#
docker build \
  --no-cache='true' \
  --tag="${REPOSITORY}/${IMAGENAME}:latest" \
  --tag="${REPOSITORY}/${IMAGENAME}:${IMAGETAG}" \
  .   # Run inside current directory
