# Security-Services/Code-Checking

This repository contains the Dockerfile with additional tools that are used
to build the **Security-Services/Code-Checking** Docker image for automated
code checking with [Gitlab-CI](https://gitlab.cern.ch/gitlabci-examples/static_code_analysis).

The image will be automatically rebuilded and pushed into [CERN's Docker
registry](http://docker.cern.ch/) each time the content of this repository
will be changed.

---


## Using Security-Services/Code-Checking image with Gitlab-CI

Please referrer to the related description at
[CERN's Gitlab-CI examples](https://gitlab.cern.ch/gitlabci-examples/static_code_analysis).

---


## Using Security-Services/Code-Checking image on local machine

Just execute the following command:
```
docker run -i -t --entrypoint /bin/bash \
       docker.cern.ch/security-services/code-checking:latest
```

---


## Building on local computer - script usage

One may be interested in building image on his/her local machine, before
commiting any changes to this repository. For this purpose, the `build.sh`
script is also distributed in this repository.

```
[fenek@Polluks Security-Services-Code-Checking]$ ./build.sh --help
Usage:
  ./build.sh [TAG]
  ./build.sh [-h|--help]

Arguments:
  TAG        -- tag string for image, current date by default
  -h|--help  -- display information about usage

Please, note that Docker service must be running on host.
```

Then deploy a container from newly created local image:
```
docker run -i -t --entrypoint /bin/bash \
       security-services/code-checking:latest
```

---


## References

\[1\]
[Automatic Docker's images building -- CERN's Gitlab-CI examples]
(https://gitlab.cern.ch/gitlabci-examples/build_docker_image)

\[2\]
[Dockerfile reference]
(https://docs.docker.com/engine/reference/builder/)

\[3\]
[Static Code Analysis with Gitlab-CI -- CERN's Gitlab-CI examples]
(https://gitlab.cern.ch/gitlabci-examples/static_code_analysis)

\[4\]
[Static Code Analysis with Gitlab-CI -- Summer Student report 2016]
(https://security.web.cern.ch/security/reports/reports/2016/Datko.pdf)

\[5\]
[Automate your life with Gitlab-CI -- Students Session 2016 (video)]
(https://cds.cern.ch/record/2206413)
